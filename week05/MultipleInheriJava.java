interface GrandParent
{
default void display()
{
System.out.println("Hi\n");
}
}
interface Parent1 extends GrandParent
{
 default void a()
 {
     System.out.println("hello\n");
 }
}
interface Parent2 extends GrandParent
{
  default void b()
  {
      System.out.println("namaskar");
  }
}
public class Child implements Parent1, Parent2
{
public static void main(String args[])
{
Child obj = new Child();
obj.display();
obj.a();
obj.b();
}
}

