//C++ program to Demonstrate 'Diamond Problem' in C++ using inheritance.
#include<iostream>
using namespace std;
class A//Grandparent class
{
    public:
    int a;
    A()
    {
        cout<<"constructor of A is called"<<endl;
    }
};
//Hierarchical inheritance
class B:virtual public A//parent class 1
{
    public:
    int b;
    B()
    {
        cout<<"constructor of B is called"<<endl;
    }
};
class C:virtual public A//parent class 2
{
    public:
    int c;
    C()
    {
        cout<<"constructor of C is called"<<endl;
    }
};
//Multiple Inheritence
class D:public B,public C//Grand child class
{
    public:
    int d;
    D()
    {
        cout<<"constructor of D is called"<<endl;
    }
};
void display(int a,int b,int c,int d)
{
    cout<<"Displaying the values :"<<endl;
    cout<<a<<" "<<b<<" "<<c<<" "<<d<<endl;
}
int main()
{
    int i1,i2,i3,i4;
    D obj;
    cout<<"Enter 4 integers :"<<endl;
    cin>>i1>>i2>>i3>>i4;
    display(i1,i2,i3,i4);
    return 0;
}