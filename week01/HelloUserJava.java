import java.util.*;
public class HelloUserJava
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter user name : ");
        String name = input.next();
        System.out.println("Hello "+name);
    }
}
/*
observation:
    Here we observed that next() or nextLine() are methods are used to take the input string from user*/